//clear or wipeout all the data inside the local storage so that the session of the user will end
localStorage.clear();
//clear() will allow you to remove the contents of the storage obj

//redirect the user to the login page, just in case a new user wants to log in
window.location.replace('./login.html');