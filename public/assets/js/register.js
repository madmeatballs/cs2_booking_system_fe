let registerUserForm = document.querySelector('#registerUser');

registerUserForm.addEventListener("submit", (e) => {
    
    e.preventDefault();

    let fName = document.querySelector('#firstName').value
    let lName = document.querySelector('#lastName').value
    let uEmail = document.querySelector('#userEmail').value
    let mNumber = document.querySelector('#mobileNumber').value
    let pw1 = document.querySelector('#password1').value
    let pw2 = document.querySelector('#password2').value

    if ((mNumber.length === 11) && (pw1 === pw2) && (pw1 !== "" && pw2 !== "")) {
        fetch('https://cryptic-hollows-99931.herokuapp.com/api/users/email-exists', {
            method: 'POST',
            headers: {
                'Content-Type':'application/json'
            },
            body: JSON.stringify({
                email: uEmail
            })
        }).then(res => {
            return res.json()   //if no duplicates found
        }).then(data => {
            console.log(data);
            if(data === false) {
                fetch("https://cryptic-hollows-99931.herokuapp.com/api/users/register", {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: fName,
                        lastName: lName,
                        email: uEmail,
                        mobileNo: mNumber,
                        password: pw1
                    })
                }).then(res => {
                    return res.json()
                }).then(data => {
                    console.log(data);
                    if(data === true) {
                        Swal.fire({
                            icon: "success",
                            title: "Beep-Bop-Beep!",
                            text: "User has been successfully registered",
                            confirmButtonText: 'Beep-Boooop!'
                        });
                    } else {
                        Swal.fire({
                            icon: "warning",
                            title: "Oh no!",
                            text: "Something went wrong!",
                            confirmButtonText: 'Try Again!'
                        });
                    }
                })
            } else {
                Swal.fire({
                    icon: 'error',
                    title: "Sorry!",
                    text:'Email already exists',
                    confirmButtonText: 'Try Again!'
                });
            }
        })
    } else {
        Swal.fire({
            icon: 'question',
            title: 'Oh no!',
            text: 'Please Insert Data First!',
            confirmButtonText: 'Try Again!'
        });
    }
})