let addCourseForm = document.querySelector('#createCourse');

addCourseForm.addEventListener("submit", (e) => {
    
    e.preventDefault(); //prevents auto redirection

    let cName = document.querySelector('#courseName').value
    let cPrice = document.querySelector('#coursePrice').value
    let cDesc = document.querySelector('#courseDescription').value

    if(cName !== " " && cPrice !== " " && cDesc !== " ") {
        fetch('https://cryptic-hollows-99931.herokuapp.com/api/courses/course-exists', {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        name:cName
    })
}).then(res => res.json()
).then(data => {
    console.log(data);
    if(data === false) {
        fetch("https://cryptic-hollows-99931.herokuapp.com/api/courses/course", {
            method: 'POST',
            headers: {
                'Content-Type':'application/json'
            },
            body: JSON.stringify({
                name: cName,
                price: cPrice,
                description: cDesc
            })
        }).then(res => {
            return res.json()
        }).then(data => {
            console.log(data);
            if(data === true) {
                Swal.fire({
                    icon: "success",
                    title: "Cool!",
                    text: "A new course has been successfully added"
                });
            } else {
                Swal.fire({
                    icon: "warning",
                    title: "Beep-booop",
                    text: "Something went wrong when adding a new course",
                    confirmButtonText: 'Try Again!'
                });
            }
        })
    } else {
        Swal.fire({
            icon: "warning",
            title: "Oh no!",
            text: `Course ${cName} already exists!`,
            confirmButtonText: 'Try Again!'
        });
    }
})
    } else {
        Swal.fire({
            icon: "warning",
            title: "Oh no!",
            text: "Something went wrong, you entered a blank course!",
            confirmButtonText: 'Try Again!'
        });
    }
})