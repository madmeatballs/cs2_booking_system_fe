let sParams = new URLSearchParams(window.location.search);
let courseId = sParams.get('courseId');
// console.log(...courseId);

console.log(sParams.has('courseId'));

let token = localStorage.getItem('token');
// console.log(token);

fetch(`https://cryptic-hollows-99931.herokuapp.com/api/courses/delete-course/${courseId}`,
    {
        method: 'DELETE',
        headers: {
            'Authorization': `Bearer ${token}`
        }
    }).then(res => {
        return res.json()
    }).then(data => {
        if(data === true) {
            Swal.fire({
                icon: 'info',
                title: 'Course Deleted!',
                text: `Course with id: ${courseId} has been deleted!`
            })
            window.location.replace('./courses.html');
        } else {
            Swal.fire({
                icon: 'error',
                title: 'Uh-oh!',
                text: `Something went wrong!`
            })
        }
    })