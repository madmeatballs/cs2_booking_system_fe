//capture the html body w/c will display the content coming form the db
let modalButton = document.querySelector('#adminButton');
let container = document.querySelector('#coursesContainer');
let cardFooter;

//take the value of the isAdmin property from the local storage
let adminCheck = localStorage.getItem('isAdmin');
let enroll = document.querySelector('#enrollmentContainer');

if(adminCheck === 'false' || !adminCheck) {
    //if a user isn't an admin do not show the addCourse button.
    modalButton.innerHTML = null;
} else {

    modalButton.innerHTML = 
        `
        <div class="col-md-2 offset-md-10"><a href="./addCourse.html" class="btn btn-block btn-dark">Add Course</a></div>
        `

}

fetch('https://cryptic-hollows-99931.herokuapp.com/api/courses/').then(res => res.json()).then(data => {
    console.log(data);

    let courseData;
    //create a control structure that will determine the value that the var courseData will hold
    if (data.length < 1) {
        courseData = "No course available"
    } else {
        //iterate the courses collection and display each course inside the browser
       courseData = data.map(course => {
            console.log(course._id);


            //if the user is a regular user, display the enroll button and display course button
            if(adminCheck == 'false' || !adminCheck) {
                cardFooter = 
                    `
                    <a href="./course.html?courseId=${course._id}" class="btn btn-dark text-white btn-block">
                    View Course Details</a>
                    `
            } else {
                cardFooter =
                    `
                    <a href="./course.html?courseId=${course._id}" class="btn btn-dark text-white btn-block">
                    View Course Details</a>

                    <a href="./editCourse.html?courseId=${course._id}" class="btn btn-dark text-white btn-block">
                    Edit Course</a>

                    <button type="button" class="btn btn-block btn-dark disableCourse" data-coursecode="${course._id}">Disable Course</button>

                    <button type="button" class="btn btn-block btn-danger delCourse" data-coursecode="${course._id}">Delete Course</button>
                    `
                    // <a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-danger text-white btn-block" id="delCourse">
                    // Delete Course</a>
            }

            return (
                `
            <div class="col-md-6 my-3">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title"> 
                        ${course.name}
                        </h5>
                        <p class="card-text text-left">
                        ${course.description}
                        </p>
                        <p class="card-text text-left">
                        ₱ ${course.price}
                        </p>
                        <p class="card-text text-left">
                        ${course.createdOn}
                        </p>
                    </div>
                    <div class="card-footer">
                        ${cardFooter}
                    </div>
                </div>
            </div>
                ` //attached a query string int he URL w/c allows us to embed the id from the database record into the query string
                // ? -> inside the URL acts as a separator, it indicates the end of a URL resource parh, and indicates the start of the query string.
            )
        }).join("") //use the join() to create a return of a new string
        //it contatenated all the objs in the array and converted each string into a data type
    }
    container.innerHTML = courseData;

}).then(() => {

        document.querySelectorAll('.delCourse').forEach(btn => btn.addEventListener("click", (e) => {
            e.preventDefault();
    
            Swal.fire({
                title: 'Are you sure you want to delete this course?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
              }).then((result) => {
                if (result.isConfirmed) {
                  Swal.fire(
                    'Deleted!',
                    'Course has been deleted.',
                    'success'
                  )
                  window.location.replace(`./deleteCourse.html?courseId=${btn.getAttribute('data-coursecode')}`);
                }
              })

            // this if function is for standard confirm
            // if(confirm('Are you sure?')) { 
            //     window.location.replace(`./deleteCourse.html?courseId=${btn.getAttribute('data-coursecode')}`);
            // } else {
            //     return null;
            // }
    
        }))

        document.querySelectorAll('.disableCourse').forEach(btn => btn.addEventListener("click", (e) => {
            e.preventDefault();
    
            Swal.fire({
                title: 'Are you sure you want to disable this course?',
                text: "Don't worry, you can re-enable this course again.",
                icon: 'info',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, disable it!'
              }).then((result) => {
                if (result.isConfirmed) {
                  Swal.fire(
                    'Disabled!',
                    'Course has been disabled.',
                    'warning'
                  )
                  window.location.replace(`./deleteCourse.html?courseId=${btn.getAttribute('data-coursecode')}`);
                }
              })

            // this if function is for standard confirm
            // if(confirm('Are you sure?')) { 
            //     window.location.replace(`./deleteCourse.html?courseId=${btn.getAttribute('data-coursecode')}`);
            // } else {
            //     return null;
            // }
    
        }))

})