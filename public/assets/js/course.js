//identify w/c course it needs to displayinside the browser
//to identify the selected course, use course id to identify the correct course properly.
let params = new URLSearchParams(window.location.search);
//window.location ->  returns a location obj with info about the "current" location of the doc.
//search -> contains the query string section of the current URL.
// URLSearchParams() -> this method/constructor creates and returns a URLSearchParams obj
//URLSearchParams -> this describes the interface that defines utility methods to work with the query string of a URL.
// new -> instantiates a user-defined obj

//params = {
//   "courseId": "....id of course that we passed"
//}
let id = params.get('courseId');
console.log(id);

let enroll = document.querySelector('#enrollmentContainer')

let adminStat = localStorage.getItem('isAdmin');
let enrolleeId = localStorage.getItem('id');
let token = localStorage.getItem('token');

//Capture the section of the  html body 

let cName = document.querySelector('#courseName');
let cDesc = document.querySelector('#courseDesc');
let cPrice = document.querySelector('#coursePrice');

fetch(`https://cryptic-hollows-99931.herokuapp.com/api/courses/${id}`).then(res => res.json()).then(data => {
    console.log(data);

    cName.innerHTML = data.name
    cDesc.innerHTML = data.description
    cPrice.innerHTML = data.price
}) //if the result is displayed in the console. It means you were able to properly pass the courseid and successfully created your fetch request.

if(adminStat === 'false' || !adminStat) {
    enroll.innerHTML = 
        `
        <a id="enrollButton" class="btn btn-dark text-white btn-block my-3">Enroll Course </a>
        `
} else {
    enroll.innerHTML = null;
}

document.querySelector('#enrollButton').addEventListener("click" , (e) => {
    e.preventDefault();

    fetch(`https://cryptic-hollows-99931.herokuapp.com/api/users/enroll`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify({
            courseId: id
        })
        }).then(res => {
            return res.json()
        }).then(data => {
            if (data === true) {
                Swal.fire({
                    icon: 'success',
                    title: `Course has been enrolled!`,
                    text: `Course with id: ${id} has been enrolled!`
                }).then(() => {
                    window.location.replace("./courses.html");
                });

            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Uh-oh!',
                    text: `Something went wrong! Make sure you are logged in!`
                }).then(() => {
                    window.location.replace("./courses.html");
                })
            }
        });


//     fetch('https://cryptic-hollows-99931.herokuapp.com/api/users/enrolled', {
//         method: 'POST',
//         headers: {
//             'Content-Type': 'application/json'
//         },
//         body: JSON.stringify({
//             userId: enrolleeId
//         })
//     }).then(res => {
//         return res.json()
//     }).then(data => {
//         console.log(data);

//         if(data === false) {
// //input code here


//         } else {
//             alert('sorry, course already enrolled!')
//         }
//     })

});