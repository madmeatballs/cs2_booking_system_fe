//capture navbar element
let navItems = document.querySelector('#navSession')
let navItems2 = document.querySelector('#navSession2')
let navItems3 = document.querySelector('#navSession3')
// console.log(navItems);
//get access to the token
let userToken = localStorage.getItem("token")

// console.log(userToken);
//create if/else for when the token exists a navbar item Log Out will be shown and if it doesn't Log in will be shown
if(!userToken) {
	navItems.innerHTML =
		`
			<li class="nav-item">
				<a href="./pages/register.html" class="nav-link"> Register </a>
			</li>
		`
	navItems2.innerHTML =
		`
			<li class="nav-item">
				<a href="./pages/login.html" class="nav-link"> Log in </a>
			</li>
		`


} else {
	navItems.innerHTML =
		`
			<li class="nav-item">
				<a href="./pages/profile.html" class="nav-link">Profile</a>
			</li>
		`
	navItems3.innerHTML = 
		`
			<li class="nav-item">
				<a href="./pages/logout.html" class="nav-link"> Log Out </a>
			</li>
		`
}