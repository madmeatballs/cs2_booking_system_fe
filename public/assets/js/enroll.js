let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId');
console.log(courseId);
let token = localStorage.getItem('token');

fetch(`https://cryptic-hollows-99931.herokuapp.com/api/users/enroll`, {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
    },
    body: JSON.stringify({
        courseId
    })
}).then(res => res.json()).then(data => {
    if (data === true) {
        alert('successfully enrolled');
    } else {
        alert('unable to enroll');
    }
})