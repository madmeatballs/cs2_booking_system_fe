//capture navbar element
let navItems = document.querySelector('#navSession');
let navItems2 = document.querySelector('#navSession2');
let navItems3 = document.querySelector('#navSession3');
let navItems4 = document.querySelector('#navSession4');
let navItems5 = document.querySelector('#navSession5');
let navItems6 = document.querySelector('#navSession6');

let navDropItem = document.querySelector('#navDropItem');
// console.log(navItems);
//get access to the token
let userToken = localStorage.getItem("token");
let isAdmin = localStorage.getItem('isAdmin');
let isLogin = localStorage.getItem('id');
// console.log(userToken);
let urlCheck = window.location.href;
//create if/else for when the token exists a navbar item Log Out will be shown and if it doesn't Log in will be shown

if (urlCheck !== "file:///C:/Users/nicol/Documents/Batch87/s18-25_Capstone_project/frontend/pages/profile.html") {
	
	if(!userToken) {
		navItems5.innerHTML = 
					`
						<li class="nav-item">
							<a href="./register.html" class="nav-link"> Register </a>
						</li>
	
					`
		navItems6.innerHTML = 
					`
						<li class="nav-item">
							<a href="./login.html" class="nav-link"> Log in </a>
						</li>
	
					`
	} else {
		navItems.innerHTML = 
					`
						<li class="nav-item active">
						<a href="./profile.html" class="nav-link"> Profile </a>
						</li>
					`

		navItems6.innerHTML = 
					`
						<li class="nav-item">
							<a href="./logout.html" class="nav-link"> Log Out </a>
						</li>
					`
	}

} else {

	if(!userToken) {
		navItems5.innerHTML = 
					`
						<li class="nav-item">
							<a href="./register.html" class="nav-link"> Register </a>
						</li>
	
					`
		navItems6.innerHTML = 
					`
						<li class="nav-item">
							<a href="./login.html" class="nav-link"> Log in </a>
						</li>
	
					`
	} else {
		navItems6.innerHTML = 
					`
						<li class="nav-item">
							<a href="./logout.html" class="nav-link"> Log Out </a>
						</li>
					`
	}
	
	if(isAdmin === 'false' || !isAdmin) {
		//if a user isn't an admin do not show the addCourse/deleteCourse/editCourse navItems.
		navDropItem.innerHTML = null;
	} else {
	
		navDropItem.innerHTML = 
			`
			<li>
			<a href="./addCourse.html" class="dropdown-item"> Add Course</a>
			</li>
			`

	}
	
	if(!isLogin || isLogin == "") {
		navItems.innerHTML = null;
	} else {
		navItems.innerHTML = 
			`
			<li class="nav-item">
			<a href="./profile.html" class="nav-link"> Profile </a>
			</li>
			`
	}

}

if (urlCheck == "file:///C:/Users/nicol/Documents/Batch87/s18-25_Capstone_project/frontend/pages/profile.html") {
	
	if(!userToken) {
		navItems5.innerHTML = 
					`
						<li class="nav-item">
							<a href="./register.html" class="nav-link"> Register </a>
						</li>
	
					`
		navItems6.innerHTML = 
					`
						<li class="nav-item">
							<a href="./login.html" class="nav-link"> Log in </a>
						</li>
	
					`
	} else {
		navItems.innerHTML = 
					`
						<li class="nav-item active">
						<a href="./profile.html" class="nav-link"> Profile </a>
						</li>
					`

		navItems6.innerHTML = 
					`
						<li class="nav-item">
							<a href="./logout.html" class="nav-link"> Log Out </a>
						</li>
					`
	}

} else {

	if(!userToken) {
		navItems5.innerHTML = 
					`
						<li class="nav-item">
							<a href="./register.html" class="nav-link"> Register </a>
						</li>
	
					`
		navItems6.innerHTML = 
					`
						<li class="nav-item">
							<a href="./login.html" class="nav-link"> Log in </a>
						</li>
	
					`
	} else {
		navItems6.innerHTML = 
					`
						<li class="nav-item">
							<a href="./logout.html" class="nav-link"> Log Out </a>
						</li>
					`
	}
	
	if(isAdmin === 'false' || !isAdmin) {
		//if a user isn't an admin do not show the addCourse/deleteCourse/editCourse navItems.
		navDropItem.innerHTML = null;
	} else {
	
		navDropItem.innerHTML = 
			`
			<li>
			<a href="./addCourse.html" class="dropdown-item"> Add Course</a>
			</li>
			`

	}
	
	if(!isLogin || isLogin == "") {
		navItems.innerHTML = null;
	} else {
		navItems.innerHTML = 
			`
			<li class="nav-item">
			<a href="./profile.html" class="nav-link"> Profile </a>
			</li>
			`
	}

}