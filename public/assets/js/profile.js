//get the access token in the local storage
let token = localStorage.getItem('token');

console.log(token);
//tgt the profile container
let profile = document.querySelector('#profileContainer');

//create a control structure that will determine the display if the access token is null or empty

if (!token || token === null) {
    //redirect the user to the login page
    alert("You must login first");
    window.location.href="./login.html"
} else {
    fetch('https://cryptic-hollows-99931.herokuapp.com/api/users/details', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    }).then(res => {
        return res.json();
    }).then(data => {
        console.log(data);
        
        let enrollmentData = data.enrollments.map(classData => {
            console.log(classData);
            return (
                `
                <tr>
                    <td>${classData.courseId}</td>
                    <td>${classData.enrolledOn}</td>
                    <td>${classData.status}</td>
                </tr>
                `
            )
        }).join("") //removes the commas

        profile.innerHTML = 
        `
        <div class="col-md-12" id="profile-cont">
        <section class="jumbotron my-5">
            <h3 class="text-center">First Name: ${data.firstName}</h3>
            <h3 class="text-center">Last Name: ${data.lastName}</h3>
            <h3 class="text-center">Email: ${data.email}</h3>
            <table class="table">
                <thead>
                    <tr>
                        <th>Course ID: </th>
                        <th>Enrolled On: </th>
                        <th>Status:</th>
                        <tbody>${enrollmentData}</tbody>
                    </tr>
                </thead>
            </table>
        </section>
        </div>
        `
    });
}
