let params = new URLSearchParams(window.location.search);
let editCourseForm = document.querySelector('#editCourse')
let id = params.get('courseId');
let idCheck = params.has('courseId');
console.log(params.has('courseId')); //rets true if exists

let courseId = params.get('courseId');

let cName = document.querySelector('#courseName');
let cDesc = document.querySelector('#courseDesc');
let cPrice = document.querySelector('#coursePrice');

let eName = document.querySelector('#editName');
let eDesc = document.querySelector('#editDesc');
let ePrice = document.querySelector('#editPrice');

// WIP
//document.querySelector('')

fetch(`https://cryptic-hollows-99931.herokuapp.com/api/courses/edit-course/${id}`, {
    method: 'PUT',
    headers: {
        'Content-Type':'application/json'
    },
    body: JSON.stringify({
        name: eName.value,
        price: ePrice.value,
        description: eDesc
    })
}).then(res => {
    return res.json()
}).then(data => {
    console.log(data);
    if(data === true) {
        alert("Successfully edited")
    } else {
        alert("Something went wrong when editing a course")
    }
})
