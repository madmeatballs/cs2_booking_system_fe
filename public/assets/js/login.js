let loginForm = document.querySelector('#loginUser')

loginForm.addEventListener('submit', (e) => {
    e.preventDefault();

    let uEmail = document.querySelector('#userEmail').value
    let uPass = document.querySelector('#password').value

    // console.log(uEmail);
    // console.log(uPass);

    //how can we inform a user that a blank form should not be allowed/blank input
    if(uEmail == "" || uPass == ""){
        Swal.fire({
            icon: "warning",
            title: "Missing information!",
            text: "Please enter your email or password!",
        })
    } else {
        fetch('https://cryptic-hollows-99931.herokuapp.com/api/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: uEmail,
                password: uPass 
            })
        }).then(res => {
            return res.json()
        }).then(data => {
            if(data.access) {
                //lets save the access token inside our local storage.
                localStorage.setItem('token', data.access)
                //this local storage can be found inside the subfolder of the appData of the local file of the web browser inside the data module of the user folder
                //alert('Access key saved on local storage')
                fetch(`https://cryptic-hollows-99931.herokuapp.com/api/users/details`, {
                    headers: {
                        'Authorization': `Bearer ${data.access}`,
                    }
                }).then(res => {
                    return res.json();
                }).then(data => { 
                    // console.log(data);
                    localStorage.setItem("id", data._id) //came from payload
                    localStorage.setItem("isAdmin", data.isAdmin)
                    // console.log('items are set inside the local storage.');
                    Swal.fire({
                        icon: "success",
                        title: "Welcome!",
                        text: "You are successfully logged in!"
                    });
                    window.location.replace('./courses.html') //directs user to the courses page after login
                })
            } else {
                //if there is no existing access key value from the data var then just inform the user.
                Swal.fire({
                    icon: "warning",
                    title: "Oh no!",
                    text: "Something went wrong, check your credentials!",
                    confirmButtonText: 'Try Again!'
                });
            }
        })
    }
})